# coding: utf-8

# ruby solve.rb 1000 < data.in.txt
# みたいなかんじで使う
# 1000というのは最初の所持金
# 定数MONEYのコメントアウトを解除すれば10000円札まで対応

MONEY = [ 1000, 500, 100, 50, 10, 5, 1 ]
#MONEY = [ 10000, 5000, 1000, 500, 100, 50, 10, 5, 1 ]
PRICE = STDIN.readlines.map(&:to_i)

class OMaster
  attr_accessor :order, :sum
  def initialize( money )
    if (t = PRICE.inject(0){|s,a|s+=a}) > money
      puts"price total(#{t}) > money(#{money}): cannot solve";exit
    end
    
    @money = money
    @order = Array.new( PRICE.size, -1 )
    @sum = 9999 # 最適解
    @cache = Hash.new # 持ち金と残りの商品をキーとして、[コスト, 順序]を保持
    @cache_m = Array.new( @money+1, nil ) # お金のリスト化もそれなりに計算コストがあるので
    @time_start = Time.now
    
    @hit = 0 # キャッシュのヒット回数
    @call = 0 # dfsの呼び出し回数
  end
  
  def solve
    # selectedはビットベクトルにしてあるけど、単純にbooleanの配列で良いかも
    dfs( depth=0, @money, selected=0, cost=0, order=Array.new( PRICE.size ) )
  end
  
  def output
    puts"best: #{@sum}"
    @order.map{|i|
      payList = getPayList( PRICE[i], @money )
      # 10000円, 5000円は表示しない
      puts( ([i]+payList).join(' ') )
      @money, cost = getCharge( PRICE[i], @money )
    }
    puts"total time: #{getTime}, cache hit: #{@hit}/#{@call}(#{@hit*100/@call}%)"
  end
  
  private
  
  # メモ化しながら深さ優先全探索
  def dfs( depth, money, selected, total_cost, order )
    n_goods = PRICE.size
    @call += 1
    
    # ノード終端
    if depth==n_goods
      renew( total_cost, order )
      return [total_cost, order.dup]
    end
    
    # キャッシュにヒット
    if (c = @cache[ [money,selected] ]) != nil
      @hit += 1
      order[depth..-1] = c[1][depth..-1]
      renew( total_cost+c[0], order )
      return [ total_cost+c[0], order.dup ]
    end
    
    # いかにもDFSな部分
    r = []
    n_goods.times{|i|
      next if selected&(1<<i) != 0
      
      left_money, cost = getCharge( PRICE[i], money )
      selected |= 1<<i
      order[depth] = i
      r << dfs( depth+1, left_money, selected, total_cost+cost, order )
      selected ^= 1<<i
    }
    
    # 一番良い解をキャッシュに
    best = r.min
    @cache[ [money,selected] ] = [best[0] - total_cost, best[1].dup]
    
    best
  end
  
  def renew( sum, order )
    if sum < @sum
      @sum = sum
      @order = order.dup
      puts"renew(#{getTime}): #{@sum} [#{@order.join(', ')}]"
    end
  end
  
  # 貨幣リストを作る
  def getMoneyList(n)
    if @cache_m[n] == nil
      @cache_m[n] = MONEY.map{|m| d, n = n.divmod(m); d }
    else
      @cache_m[n]
    end
  end
  
  # 所持金と代金から、所持金・支払い後の所持金・それらの共通部分のリストを得る
  def get3Lists( money, price )
    [ money_list = getMoneyList( money ),
      left_money_list = getMoneyList( money - price ),
      and_list = getAndList( money_list, left_money_list ) ]
  end
  
  # 共通部分
  def getAndList( la, lb )
    (0..MONEY.size-1).map{|i| [ la[i], lb[i] ].min }
  end
  
  # (la - lb)の合計を計算している
  def getCost( la, lb )
    (0..MONEY.size-1).inject(0){|s,i| s += la[i] - lb[i] }
  end
  
  # 残りのお金とお釣りの枚数を返す
  def getCharge( price, money )
    money_list, left_money_list, and_list = get3Lists( money, price )
    cost = getCost( left_money_list, and_list )
    [ (money - price), cost ]
  end
  
  # 解答出力用の支払いリストを得る
  def getPayList( price, money )
    money_list, left_money_list, and_list = get3Lists( money, price )
    (0..MONEY.size-1).map{|i| money_list[i] - and_list[i] }
  end
  
  def getTime
    "#{((Time.now-@time_start)*1000).to_i}ms"
  end
end

# 初期金額は引数で与えることにしておく
om = OMaster.new( ARGV[0].to_i )
om.solve
om.output
